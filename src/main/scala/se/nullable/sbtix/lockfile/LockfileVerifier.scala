package se.nullable.sbtix.lockfile

import coursier.Cache
import java.io.{File, IOException}
import java.nio.file.Path

class LockfileVerifier(lockfile: Lockfile) {
  val downloading = collection.mutable.Map[String, Path]()

  def verifyExists(url: String,
                   absolutePath: Path,
                   cachePath: Path): LockfileEntry = {
    val relativePath = cachePath.relativize(absolutePath)
    lockfile.files
      .get(relativePath)
      .getOrElse(
        throw new IOException(
          s"Cache path $relativePath ($url) is not listed in the lockfile")
      )
  }

  def verify(url: String, absolutePath: Path, cachePath: Path): Unit = {
    val entry = verifyExists(url, absolutePath, cachePath)
    LockfileBuilder.verify(entry, absolutePath)
  }

  def logger(cache: Path, innerLogger: Cache.Logger) =
    new CoursierLoggerWrapper(innerLogger) {
      override def foundLocally(url: String, f: File): Unit = {
        super.foundLocally(url, f)
        verify(url, f.toPath(), cache)
      }
      override def downloadingArtifact(url: String, file: File): Unit = {
        super.downloadingArtifact(url, file)
        verifyExists(url, file.toPath(), cache)
        downloading += url -> file.toPath()
      }
      override def downloadedArtifact(url: String, success: Boolean): Unit = {
        super.downloadedArtifact(url, success)
        val path = downloading.remove(url).get
        verify(url, path, cache)
      }
    }

}
