if (sys.props.contains("plugin.version")) {
  Seq(addSbtPlugin("se.nullable.sbtix" % "sbt-lockfile" % sys.props("plugin.version")))
} else {
  Seq()
}

// We need the Coursier snapshot
// Disabled since we import it as a source dependency
// resolvers += Resolver.sonatypeRepo("snapshots")
