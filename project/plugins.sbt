resolvers += Resolver.typesafeIvyRepo("releases")

libraryDependencies += "org.scala-sbt" %% "scripted-plugin" % sbtVersion.value

addSbtPlugin("io.get-coursier" % "sbt-coursier" % "1.1.0-M5")
addSbtPlugin("com.lucidchart" % "sbt-scalafmt" % "1.15")
addSbtPlugin("com.jsuereth" % "sbt-pgp" % "1.1.1")
